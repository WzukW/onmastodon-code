# On Mastodon

> Due to legal concern (indexing profile is regulated as personnal data handling under French law), the original index of “influential” profiles has been shutdown. However, the code of the website is here, in the hope to be useful to someone else.
> You may find example data in `yml-db` folder. Any resemblance to real and actual names is purely coincidental.

[![Website](https://img.shields.io/website-up-down-green-red/http/onmastodon.tk.svg?label=onmastodon.tk)](https://onmastodon.tk)
[![build status](https://gitlab.com/WzukW/onmastodon-code/badges/master/build.svg)](https://gitlab.com/WzukW/onmastodon/commits/master)

This is a list of popular Twitter account or media you can find on [Mastodon](https://joinmastodon.org).

> Pull request are welcome!

#### Build

We use [Nanoc](https://nanoc.ws).


1. [Install](https://nanoc.ws/doc/installation/) Nanoc
1. Install all the dependencies: `bundle install`
1. Generate the website: `nanoc`
1. Preview: `nanoc view`
1. Add or edit content

Read more at Nanoc's [documentation](https://nanoc.ws/doc/)
