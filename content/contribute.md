---
title: Contribute
---
# Contribute

Contributions are welcome, on [Github](https://github.com/leowzukw/onmastodon) or [Gitlab](https://gitlab.com/WzukW/onmastodon)!

Here is some advices. If you need help, [contact us](/about).

## Adding Profiles

### Rules

Listed profiles must:

 +  Have at least 100,000 follower on Twitter or 500 on Mastodon.
 +  Be active, almost as their Twitter counterparts, if any.
 +  Mention if they are bot.

In case several bots for the same entity are available on different instances, you might list several.

Things should be alphabetically or randomly organised if nothing else makes sense.

### Yaml Files

Each profile is stored in a [Yaml](http://yaml.org) file of its own. Name it without any reference to any mastodon instance, if possible: for instance `snowden.yml` instead of `snowden.mamot.fr.yml`. The thing is there may be several instances listed in mastdonUrls field.

It has the following structure:

<table class="pure-table pure-table-bordered pure-table-odd">
  <thead>
    <tr>
      <th>Field</th>
      <th>Type</th>
      <th>Required</th>
      <th>Description</th>
      <th>Example</th>
    </tr>
  </thead>
  <tbody>
  <tr>
      <td>name</td>
      <td>string</td>
      <td>✔</td>
      <td>Human readable name of the profile</td>
      <td>E. Snowden</td>
    </tr>
    <tr>
      <td>description</td>
      <td>string</td>
      <td>✗</td>
      <td>Short description of the profile, you may summurize the profile description</td>
      <td>President at Freedom of Press.</td>
    </tr>
    <tr>
      <td>bot</td>
      <td>bool</td>
      <td>✔</td>
      <td>Whether the profile is a bot</td>
      <td>true</td>
    </tr>
    <tr>
      <td>lang</td>
      <td>string list</td>
      <td>✔</td>
      <td>List of languages used by the profile</td>
      <td> - en<br/> - fr</td>
    </tr>
    <tr>
      <td>mastodonUrls</td>
      <td>string list</td>
      <td>✔</td>
      <td>A list of urls for the profile (accross instances)</td>
      <td> - https://mamot.fr/@snowden<br/> - https://mastodon.xyz/@snowden</td>
    </tr>
    <tr>
      <td>twitterUrls</td>
      <td>string list</td>
      <td>✗</td>
      <td>A list of urls for the profile (accross instances)</td>
      <td> - https://twitter.com/Snowden</td>
    </tr>
    <tr>
      <td>runBy</td>
      <td>string list</td>
      <td>✗</td>
      <td>A list of urls of associated ressources (Newspaper website, personnal blog)</td>
      <td> - https://snowden.blog</td>
    </tr>
    </tbody>
    </table>

There is a [template](/template.yml) to get started. You might browse [the source code](https://gitlab.com/WzukW/onmastodon/tree/master/yml-db) to find examples.

### Your pull request

+ You should make per-profile pull-request (PR): if your PR is rejected for one profile, the others are not excluded.
+ You might mention your mastodon account.
+ You should entitle it *Profile: …* to keep things tidy.
+ You should use English in your comments and description.
+ You should give number of follower to ease verification of rules.

## Edit the website

Found a typo? Have ideas to improve a page? Here is a few tips:

 + We use [Nanoc](https://nanoc.ws).
 + Pages are stored in [content](https://gitlab.com/WzukW/onmastodon/tree/master/content), if they are not Mastodon profiles.
 + Templates are in [layouts](https://gitlab.com/WzukW/onmastodon/tree/master/layouts).

* * * * * * * *

## Contributor Covenant Code of Conduct

### Our Pledge

In the interest of fostering an open and welcoming environment, we as
contributors and maintainers pledge to making participation in our project and
our community a harassment-free experience for everyone, regardless of age, body
size, disability, ethnicity, gender identity and expression, level of experience,
nationality, personal appearance, race, religion, or sexual identity and
orientation.

### Our Standards

Examples of behavior that contributes to creating a positive environment
include:

* Using welcoming and inclusive language
* Being respectful of differing viewpoints and experiences
* Gracefully accepting constructive criticism
* Focusing on what is best for the community
* Showing empathy towards other community members

Examples of unacceptable behavior by participants include:

* The use of sexualized language or imagery and unwelcome sexual attention or
advances
* Trolling, insulting/derogatory comments, and personal or political attacks
* Public or private harassment
* Publishing others' private information, such as a physical or electronic
  address, without explicit permission
* Other conduct which could reasonably be considered inappropriate in a
  professional setting

### Our Responsibilities

Project maintainers are responsible for clarifying the standards of acceptable
behavior and are expected to take appropriate and fair corrective action in
response to any instances of unacceptable behavior.

Project maintainers have the right and responsibility to remove, edit, or
reject comments, commits, code, wiki edits, issues, and other contributions
that are not aligned to this Code of Conduct, or to ban temporarily or
permanently any contributor for other behaviors that they deem inappropriate,
threatening, offensive, or harmful.

### Scope

This Code of Conduct applies both within project spaces and in public spaces
when an individual is representing the project or its community. Examples of
representing a project or community include using an official project e-mail
address, posting via an official social media account, or acting as an appointed
representative at an online or offline event. Representation of a project may be
further defined and clarified by project maintainers.

### Enforcement

Instances of abusive, harassing, or otherwise unacceptable behavior may be
reported by contacting the project team at [@onmastodon@mamot.fr](https://mamot.fr/@onmastodon). All
complaints will be reviewed and investigated and will result in a response that
is deemed necessary and appropriate to the circumstances. The project team is
obligated to maintain confidentiality with regard to the reporter of an incident.
Further details of specific enforcement policies may be posted separately.

Project maintainers who do not follow or enforce the Code of Conduct in good
faith may face temporary or permanent repercussions as determined by other
members of the project's leadership.

### Attribution

This Code of Conduct is adapted from the [Contributor Covenant][homepage], version 1.4,
available at [http://contributor-covenant.org/version/1/4][version]

[homepage]: http://contributor-covenant.org
[version]: http://contributor-covenant.org/version/1/4/
